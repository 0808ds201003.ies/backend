//mongodb connection to atlas
import  mongoose from 'mongoose'
import  dotenv from 'dotenv'
dotenv.config()

const connectDB=async()=>{
   try {
    const conn=await mongoose.connect(process.env.MONGO_URL)
    console.log("Connected to mongodb database");
    
   } catch (error) {
    console.log(`Error occured in mongodb ${error}`);
    
   }
    
}
export default connectDB;