import { comparePassword, hashPassword } from "../helper/authHelper.js";
import userModel from "../models/userModel.js";
import orderModel from "../models/orderModel.js";
import JWT from "jsonwebtoken";

export const registerController = async (req, res) => {
  try {
    //taking all the inputs from the request of the user
    const { name, email, password, mobile, address, answer } = req.body;
    if (!name) {
      return res.send({ error: "Name is requires" });
    }
    if (!email) {
      return res.send({ error: "Email is requires" });
    }
    if (!password) {
      return res.send({ error: "Password is requires" });
    }
    if (!mobile) {
      return res.send({ error: "Mobile is requires" });
    }
    if (!address) {
      return res.send({ error: "Address is requires" });
    }
    if (!answer) {
      return res.send({ error: "Answer is requires" });
    }

    //checking if user already exists in the database or not
    const existingUser = await userModel.findOne({ email });
    if (existingUser) {
      res.status(200).send({
        success: false,
        message: "User already refistered",
      });
    }
    //if user is not alrady registered in the database the we need to add him into the database

    //for this the first step is to hash the password
    const hashedPassword = await hashPassword(password);

    //creating an object of usermodel to save in the database
    const user = await new userModel({
      name,
      email,
      password: hashedPassword,
      mobile,
      address,
      answer,
    }).save();

    res.status(200).send({
      success: true,
      message: "User registered successfully",
      user,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      success: false,
      message: "Error in User Registration",
      error,
    });
  }
};

export const loginController = async (req, res) => {
  try {
    const { email, password } = req.body;
    if (!email) {
      return res.send({ error: "Email is requires" });
    }
    if (!password) {
      return res.send({ error: "Password is requires" });
    }
    //checking if the user in available in the database or not
    const user = await userModel.findOne({ email });
    if (!user) {
      return res.status(404).send({
        success: false,
        message: "user is not registeresd",
      });
    }
    //checking if the password given is same as in the db or not
    //password:given by user
    //user.password:password from the database when user is fetched
    const match = await comparePassword(password, user.password);
    if (!match) {
      res.status(401).send({
        success: false,
        message: "password did not match",
      });
    }
    //now if user is logged in then generating token
    const token = await JWT.sign({ _id: user._id }, process.env.SECRET_KEY, {
      expiresIn: "5d",
    });
    res.status(200).send({
      success: true,
      message: "login successfully",
      user: {
        _id: user._id,
        name: user.name,
        email: user.email,
        mobile: user.mobile,
        role: user.role,
        address: user.address,
      },
      token,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      success: false,
      message: "Error in Login",
      error,
    });
  }
};

export const forgotPasswordController = async (req, res) => {
  try {
    const { email, answer, newPassword } = req.body;
    if (!email) {
      res.status(400).send({ error: "Email is requires" });
    }
    if (!answer) {
      res.status(400).send({ error: "Answer is requires" });
    }
    if (!newPassword) {
      res.status(400).send({ error: "New Password is requires" });
    }
    //user will be found using the email and answer he shared during the registration
    const user = await userModel.findOne({ email, answer });
    if (!user) {
      return res.status(404).send({
        success: false,
        message: "wrong user and answer",
      });
    }
    //password is hashed first
    const hashed = await hashPassword(newPassword);
    //then user is updated
    await userModel.findByIdAndUpdate(user._id, {
      password: hashed,
    });
    res.status(200).send({
      success: true,
      message: "password updated sucessfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      success: false,
      message: "Error in Login",
      error,
    });
  }
};

//orders
export const getOrderController = async () => {
  try {
    //here order model got reference from the product and buyer model so we use populate method to show the data of respective data model
    const orders = await orderModel
      .find({ buyer: req.user_id })
      .populate("products", "-photo")
      .populate("buyer", "name");
    res.json(orders);
  } catch (error) {
    console.log(error);
    res.status(500).send({
      success: false,
      message: "Error in getting all orders",
      error,
    });
  }
};
