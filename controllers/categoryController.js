import categoryModel from "../models/categoryModel.js";
import slugify from "slugify";


//to create a new category ->specfic for admin
export const createCategoryController = async (req, res) => {
  try {
    const { name } = req.body;
    if (!name) {
      return res.status(401).send({ message: "Name is require" });
    }
    const existingCategory = await categoryModel.findOne({ name });
    if (existingCategory) {
      res.status(200).send({
        success: false,
        message: "category already exists",
      });
    }
    const category = await new categoryModel({
      name,
      slug: slugify(name),
    }).save();

    res.status(201).send({
      success: true,
      message: "New category created",
      category,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      success: false,
      message: "Error in creation of categoy",
      error,
    });
  }
};

//to get all category->all user can access it
export const getCategoryController=async(req,res)=>{
    try {
        const categories=await categoryModel.find();
        res.status(201).send({
            success:true,
            message:"All categories",
            categories
        })
        
    } catch (error) {
       console.log(error);
    res.status(500).send({
      success: false,
      message: "Error in getting categoy",
      error,
    });
        
    }
}
//to get single category->all user can access it
export const singleCategoryController=async(req,res)=>{
    try {
        const category=await categoryModel.findOne({slug:req.params.slug})
        res.status(200).send({
            success:true,
            message:"Specfic categories",
            category,
        })
        
    } catch (error) {
       console.log(error);
    res.status(500).send({
      success: false,
      message: "Error in getting categoy",
      error,
    });
        
    }
}


//update category
export const updateCategoryController=async(req,res)=>{
    try {
        const {name}=req.body;
        const {id}=req.params
        const category=await categoryModel.findByIdAndUpdate(
            id,
            {name,slug:slugify(name)},
            {new:true}
        )
        res.status(200).send({
            success:true,
            message:"category updated successfully",
            category
        })
    } catch (error) {
        console.log(error);
        res.status(500).send({
          success: false,
          message: "Error in updating categoy",
          error,
        });
        
    }

       
}

//delete category
export const deleteCategoryController=async(req,res)=>{
    try {
        const {id}=req.params;
        await categoryModel.findByIdAndDelete(id)
        res.status(200).send({
            success:true,
            message:"Category deleted successfully",   
        })

        
    } catch (error) {
        console.log(error);
      res.status(500).send({
      success: false,
      message: "Error in deleting categoy",
      error,
    });
        
    }
}
