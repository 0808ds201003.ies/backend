//dependies
import  express from'express'
import  dotenv from 'dotenv'
import  connectDB from './config/db.js'
import authRoute from './routes/authRoute.js'
import categoryRoute from './routes/categoryRoute.js'
import productRoute from './routes/productRoute.js'
import  bodyParser from "body-parser"





//database connection
connectDB()
//creation of app
const app=express()

//configuring the dotenv file
dotenv.config()


//using the middleware for parsing the data and json file
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }))
//setting  up the routes
app.use('/api/v1/auth',authRoute)
app.use('/api/v1/category',categoryRoute)
app.use('/api/v1/product',productRoute);

const PORT=process.env.PORT

//listining to the port
app.listen(PORT,()=>{
    console.log(`server running on server ${PORT}`);
})