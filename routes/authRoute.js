
import express from 'express'
import {registerController,loginController,forgotPasswordController} from '../controllers/authController.js'

const router=express.Router()


//creating the routes for login and register


//router for register
router.post('/register',registerController)

//router for login
router.post('/login',loginController)

//router for update
router.put('/forgot-password',forgotPasswordController)

//routes for watching all orders
router.get('/')


export default router;

