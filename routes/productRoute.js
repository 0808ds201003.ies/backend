import express from 'express'
import { createProductController,deleteProductController,getProductController,getSingleProductController, productPhotoController, updateProductController } from '../controllers/productController.js'
import {isAdmin,requireSignIn} from '../middleware/authMiddleware.js'
import formidable from 'express-formidable'
import { updateCategoryController } from '../controllers/categoryController.js'

const router=express.Router()

//creating a product
router.post('/create-product',requireSignIn,isAdmin,formidable(),createProductController)

//getting all products
router.get('/get-product',getProductController)

//getting single product
router.get('/get-product/:slug',getSingleProductController)

//getting photo of the products
router.get('/product-photo/:pid',productPhotoController)

//delete the product
router.delete('/delete-product/:pid',requireSignIn,isAdmin,deleteProductController);

//update the product
router.put('/update-product/:pid',requireSignIn,isAdmin,formidable(),updateProductController)





export default router;