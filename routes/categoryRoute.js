import express from 'express'
import {createCategoryController,deleteCategoryController,getCategoryController,singleCategoryController, updateCategoryController} from '../controllers/categoryController.js'
import {isAdmin,requireSignIn} from '../middleware/authMiddleware.js'
const router=express.Router()


router.post('/create-category',requireSignIn,isAdmin,createCategoryController)

router.get('/get-category',getCategoryController)

router.get('/single-category/:slug',singleCategoryController)

router.delete('/delete-category/:id',requireSignIn,isAdmin,deleteCategoryController)

router.put('/update-category/:id',requireSignIn,isAdmin,updateCategoryController)


export default router;
