import bcrypt from 'bcrypt'


//for hashing the password using bcrpty
export const hashPassword=async(password)=>{
    try {
    const saltRounds = 10;
    const hashedPassword=await bcrypt.hash(password,saltRounds)
    return hashedPassword
    } catch (error) {
        console.log(error);
    }

}

//for compairing the password
export const comparePassword=async(password,hashedPassword)=>{
    return bcrypt.compare(password,hashedPassword)

}